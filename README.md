# Teste melhor envio - Desenvolvedor Backend

### Configuração

1. Após clonar o projeto executar o comando abaixo para instalar os pacotes do laravel

```shell
    componser install
```

2. Copiar o arquivo .envexample para .env.

3. Configurar no .env a conexão com o banco de dados, informando o nome do banco, usuário e senha.

4. Gerar APP_KEY

```shell
    php artisan key:generate
```

5. Após a instalação de todas as dependencias e com o banco de dados configurado, executar o comando para criar as tabelas do banco de dados. 

```shell
    php artisan migrate
```

6. Executando o projeto

```shell
    php artisan serve
```


### Rotas

##### Processar o arquivo de log e salvar dados no banco

```http
  GET /logs
```
##### Gerar relatório de Requisições por consumidor

```http
  GET /consumer
```

##### Gerar relatório de Requisições por serviços

```http
  GET /service
```

##### Gerar relatório de Tempo médio de request , proxy e kong por serviço.

```http
  GET /service_media
```


