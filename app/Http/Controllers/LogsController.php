<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\Method;
use App\Models\Path;
use App\Models\Protocol;
use App\Models\Log;
use App\Models\Route;
use App\Models\RouteMethod;
use App\Models\RouteProtocol;
use App\Models\Service;



class LogsController extends Controller
{
    protected  $method, $path, $protocol, $log, $route, $routeMethod, $routeProtocol, $service;


    function __construct(
        Method $method,
        Path $path,
        Protocol $protocol,
        Log $log,
        Route $route,
        RouteMethod $routeMethod,
        RouteProtocol $routeProtocol,
        Service $service
    ) {
        $this->method = $method;
        $this->path = $path;
        $this->protocol = $protocol;
        $this->log = $log;
        $this->route = $route;
        $this->routeMethod = $routeMethod;
        $this->routeProtocol = $routeProtocol;
        $this->service = $service;
    }

    // Ler arquivo log.txt

    public function index()
    {
        $inicio = microtime(true);

        ini_set('max_execution_time', 180);

        $exists = file_exists(public_path() . "/logs.txt");

        if ($exists) {
            $contents = file_get_contents(public_path() . "/logs.txt");
            $rows = explode("\n", $contents);

            $services = array();
            $routes = array();

            $total = count($rows); // total de linhas encontradas no arquivo
            $limit = 1000; //limite de requisições por vez

            $logs = array();

            for ($i = 0; $i < $total; $i++) {
                $row = json_decode($rows[$i], true);

                if ($row != null) {

                    // Montando array de services para salvar no futuro
                    $services[$row["service"]["id"]] = $row["service"];

                    // Montando array de routes para salvar no futuro
                    $route = $row["route"];
                    $route["service_id"] = $row["route"]["service"]["id"];
                    $routes[$row["route"]["id"]] = $route;


                    // adicionando requisição no array para salvar
                    $logs[] = [
                        "upstream_uri" => $row["upstream_uri"],
                        "client_ip" => $row["client_ip"],
                        "started_at" => $row["started_at"],
                        "proxy" => $row["latencies"]["proxy"],
                        "kong" => $row["latencies"]["kong"],
                        "request" => $row["latencies"]["request"],
                        "route_id" => $row["route"]["id"],
                        "response_status" => $row["response"]["status"],
                        "response_size" => $row["response"]["size"],
                        "response_headers" => json_encode($row["response"]["headers"]),
                        "request_size" => $row["request"]["size"],
                        "request_method" => $row["request"]["method"],
                        "request_uri" => $row["request"]["uri"],
                        "request_url" => $row["request"]["url"],
                        "request_size" => $row["request"]["size"],
                        "request_querystring" => json_encode($row["request"]["querystring"]),
                        "request_headers" => json_encode($row["request"]["headers"]),
                        "consumer_id" => $row["authenticated_entity"]["consumer_id"]["uuid"],
                    ];
                }
                // salvando requisições $limit por vez
                if ($i % $limit == 0 || $i == $total - 1) {
                    $this->log->insert($logs);
                    $logs = array();
                }
            }

            // salvando os services encontrados
            foreach ($services as $key => $service) {
                $this->service->firstOrCreate($service);
            }

            // salvando as routes encontradas
            foreach ($routes as $key => $route) {
                $methods_id = array();
                $protocols_id = array();
                $path_id = array();
                // salvando methods das routes
                foreach ($route["methods"] as $key => $value) {
                    $method = $this->method->firstOrCreate(["method" => $value]);
                    $methods_id[] = $method->id;
                }
                // salvando protocols das routes
                foreach ($route["protocols"] as $key => $value) {
                    $protocol = $this->protocol->firstOrCreate(["protocol" => $value]);
                    $protocols_id[] = $protocol->id;
                }
                // salvando paths das routes
                foreach ($route["paths"] as $key => $value) {
                    $path = $this->path->firstOrCreate(["path" => $value]);
                    $paths_id[] = $path->id;
                }

                $route = $this->route->firstOrCreate(
                    ["id" => $route["id"]],
                    [
                        "id" => $route["id"],
                        "hosts" => $route["hosts"],
                        "regex_priority" => $route["regex_priority"],
                        "preserve_host" => $route["preserve_host"],
                        "strip_path" => $route["strip_path"],
                        "created_at" => $route["created_at"],
                        "updated_at" => $route["updated_at"],
                        'service_id' => $route["service_id"],
                    ]
                ); // cadastrar route

                $route->methods()->sync($methods_id); //atualizar ou cadastrar methods para route
                $route->paths()->sync($paths_id); //atualizar ou cadastrar paths para route
                $route->protocols()->sync($protocols_id); //atualizar ou cadastrar protocols para route
            }


            $fim = microtime(true);
            return response()->json(["msg" => "Fim da importação", "time" => 'Tempo de execução: ' . ($fim - $inicio)]);
        } else {
            $fim = microtime(true);
            return response()->json(["msg" => "Arquivo não contrado", "time" => 'Tempo de execução: ' . ($fim - $inicio)]);
        }
    }


    public function format($log)
    {

        return [
            "request" => [
                "method" => $log->request_method,
                "uri" => $log->uri,
                "url" => $log->request_url,
                "size" => $log->request_size,
                "querystring" => json_decode($log->request_querystring, true),
                "headers" => json_decode($log->request_headers, true)
            ],
            "upstream_uri" => $log->upstream_uri,
            "response" => [
                "status" => $log->response_status,
                "size" => $log->response_size,
                "headers" => json_decode($log->response_headers, true)
            ],
            "authenticated_entity" => [
                "consumer_id" => [
                    "uuid" => $log->consumer_id
                ]
            ],
            "route" => [
                "created_at" => $log->route->created_at,
                "hosts" => $log->route->hosts,
                "id" => $log->route->id,
                "methods" => [],
                "paths" => [],
                "preserve_host" => $log->route->preserve_host,
                "protocols" => [],
                "regex_priority" => $log->route->regex_priority,
                "service" => $log->route->service->id,
                "strip_path" => $log->route->strip_path,
                "updated_at" => $log->route->updated_at,
            ],
            "service" => $log->route->service()->first()->toArray(),
            "latencies" => [
                "proxy" => $log->proxy,
                "kong" => $log->kong,
                "request" => $log->request,
            ],
            "client_ip" => $log->client_ip,
            "started_at" => $log->started_at
        ];


        dd($log, $logFormat);
    }


    public function consumer()
    {
        $logs = $this->log->get();
        $logs_consumer = $logs->groupBy("consumer_id");

        $linhas[] = array("consumidor", "requisicoes");

        foreach ($logs_consumer as $key => $consumer) {

            $linhas[] = array($key, count($consumer));
        }
        $name = strtotime(date("Y-m-d h:i:s"));
        $fp = fopen("consumers_$name.csv", 'w');

        foreach ($linhas as $linha) {
            fputcsv($fp, $linha);
        }

        fclose($fp);

        $file = public_path() . "/consumers_$name.csv";

        $headers = array(
            'Content-Type: application/csv',
        );

        return response()->download($file, "consumers_$name.csv", $headers);
    }

    public function service_media()
    {
        $services = $this->service->all();


        $linhas[] = array("service", "proxy", "kong", "request");

        foreach ($services as $key => $service) {
            $linhas[] = array($service->id, $service->mediaProxy(), $service->mediaKong(), $service->mediaRequest());
        }

        $name = strtotime(date("Y-m-d h:i:s"));
        $fp = fopen("services_media_$name.csv", 'w');

        foreach ($linhas as $linha) {
            fputcsv($fp, $linha);
        }

        fclose($fp);

        $file = public_path() . "/services_media_$name.csv";

        $headers = array(
            'Content-Type: application/csv',
        );

        return response()->download($file, "services_media_$name.csv", $headers);
    }

    public function service()
    {
        $services = $this->service->all();
        $linhas[] = array("servico", "requisicoes");

        foreach ($services as $key => $service) {
            $logs = $this->log->whereHas("route", function ($route) use ($service) {
                $route->where("service_id", $service->id);
            })->get();
            $linhas[] = array($service->id, count($logs));
        }

        $name = strtotime(date("Y-m-d h:i:s"));
        $fp = fopen("service_$name.csv", 'w');

        foreach ($linhas as $linha) {
            fputcsv($fp, $linha);
        }

        fclose($fp);

        $file = public_path() . "/service_$name.csv";

        $headers = array(
            'Content-Type: application/csv',
        );

        return response()->download($file, "service_$name.csv", $headers);
    }
}
