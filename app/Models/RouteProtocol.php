<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RouteProtocol extends Model
{
    use HasFactory;

    protected $table = "route_protocol";

    public $timestamps = false;

    protected $fillable = [
        "route_id",
        "protocol_id",
    ];

    function route(){
        return $this->belongsTo(Route::class);
    }

    function protocol(){
        return $this->belongsTo(Protocol::class);
    }
}
