<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Method extends Model
{
    use HasFactory;

    use HasFactory;

    protected $table = "methods";

    public $timestamps = false;

    protected $fillable = [
        "id",
        "method",
    ];
}
