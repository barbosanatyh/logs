<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Protocol extends Model
{
    use HasFactory;

    protected $table = "protocols";
    
    public $timestamps = false;

    protected $fillable = [
        "id",
        "protocol",
    ];
}
