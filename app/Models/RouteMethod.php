<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RouteMethod extends Model
{
    use HasFactory;

    protected $table = "route_method";

    public $timestamps = false;

    protected $fillable = [
        "route_id",
        "method_id",
    ];

    function route(){
        return $this->belongsTo(Route::class);
    }

    function method(){
        return $this->belongsTo(Method::class);
    }
}
