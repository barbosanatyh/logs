<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    use HasFactory;

    protected $table = "routes";

    protected $fillable = [
        "id",
        "hosts",
        "regex_priority",
        "preserve_host",
        "strip_path",
        "created_at",
        "updated_at",
        'service_id',
    ];

    protected $primaryKey = 'id';

    public $incrementing = false;

    protected $keyType = 'string';

    function service()
    {
        return $this->belongsTo(Service::class);
    }

    function protocols()
    {
        return $this->belongsToMany(RouteProtocol::class, "route_protocol", "route_id", "protocol_id");
    }

    function paths()
    {
        return $this->belongsToMany(RoutePath::class, "route_path", "route_id", "path_id");
    }

    function methods()
    {
        return $this->belongsToMany(RouteMethod::class, "route_method", "route_id", "method_id");
    }

    function logs()
    {
        return $this->hasMany(Log::class, "route_id", "id");
    }
}
