<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $table = "services";

    public $timestamps = false;

    protected $fillable = [
        "id",
        "proxy",
        "gateway",
        "request",
        "connect_timeout",
        "created_at",
        "host",
        "name",
        "path",
        "port",
        "protocol",
        "read_timeout",
        "retries",
        "updated_at",
        "write_timeout",
    ];


    protected $primaryKey = 'id';

    public $incrementing = false;

    protected $keyType = 'string';

    public function routes(){
        return $this->hasMany(Route::class, "service_id", "id");
    }
    
    public function mediaProxy(){

        $routes = $this->routes()->get();

        $total_proxy = 0;
        $total_requisicoes = 0;

        foreach ($routes as $key => $route) {
            $total_requisicoes += count($route->logs);
            foreach ($route->logs as $key => $log) {
                $total_proxy +=$log->proxy;
            }
        }
        
        return $total_proxy/$total_requisicoes;
    }

    public function mediaKong(){

        $routes = $this->routes()->get();

        $total_kong = 0;
        $total_requisicoes = 0;

        foreach ($routes as $key => $route) {
            $total_requisicoes += count($route->logs);
            foreach ($route->logs as $key => $log) {
                $total_kong +=$log->kong;
            }
        }
        
        return $total_kong/$total_requisicoes;
    }

    public function mediaRequest(){

        $routes = $this->routes()->get();

        $total_request = 0;
        $total_requisicoes = 0;

        foreach ($routes as $key => $route) {
            $total_requisicoes += count($route->logs);
            foreach ($route->logs as $key => $log) {
                $total_request +=$log->request;
            }
        }
        
        return $total_request/$total_requisicoes;
    }

}
