<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    protected $table = "logs";

    public $timestamps = false;

    protected $fillable = [
        "upstream_uri",
        "client_ip",
        "started_at",
        'route_id',
        'proxy',
        'kong',
        'request',
        'consumer_id',

        "request_method",
        "request_uri",
        "request_url",
        "request_querystring",
        "request_size",
        "request_headers",

        "response_status",
        "response_size",
        "response_headers"
    ];

    function route(){
        return $this->belongsTo(Route::class);
    }
}
