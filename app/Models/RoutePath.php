<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoutePath extends Model
{
    use HasFactory;

    protected $table = "route_path";

    public $timestamps = false;

    protected $fillable = [
        "route_id",
        "path_id",
    ];

    function route(){
        return $this->belongsTo(Route::class);
    }

    function path(){
        return $this->belongsTo(Path::class);
    }
}
