<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('latencies', function (Blueprint $table) {
        //     $table->increments("id");
        //     $table->integer("proxy");
        //     $table->integer("kong");
        //     $table->integer("request");
        // });

        Schema::create('services', function (Blueprint $table) {
            $table->string("id")->primary();
            $table->integer("connect_timeout");
            $table->string("created_at");
            $table->string("host");
            $table->string("name");
            $table->string("path");
            $table->integer("port");
            $table->string("protocol");
            $table->integer("read_timeout");
            $table->integer("retries");
            $table->string("updated_at");
            $table->integer("write_timeout");
        });

        // Schema::create('headers', function (Blueprint $table) {
        //     $table->increments("id");
        //     $table->string("tag");
        // });


        Schema::create('protocols', function (Blueprint $table) {
            $table->increments("id");
            $table->string("protocol");
        });

        Schema::create('methods', function (Blueprint $table) {
            $table->increments("id");
            $table->string("method");
        });

        Schema::create('paths', function (Blueprint $table) {
            $table->increments("id");
            $table->string("path");
        });

        Schema::create('routes', function (Blueprint $table) {
            $table->string("id")->primary();
            $table->string("hosts");
            $table->string("regex_priority");
            $table->boolean("preserve_host")->default(false);
            $table->boolean("strip_path")->default(false);
            $table->string("created_at");
            $table->string("updated_at");


            $table->string('service_id')->references('id')->on('services');
        });


        Schema::create('route_protocol', function (Blueprint $table) {
            $table->string('route_id')->references('id')->on('routes');
            $table->integer('protocol_id')->references('id')->on('protocols');
        });

        Schema::create('route_method', function (Blueprint $table) {
            $table->string('route_id')->references('id')->on('routes');
            $table->integer('method_id')->references('id')->on('methods');
        });

        Schema::create('route_path', function (Blueprint $table) {
            $table->string('route_id')->references('id')->on('routes');
            $table->integer('path_id')->references('id')->on('paths');
        });


        // Schema::create('authenticated_entity', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('consumer_id');
        // });

        // Schema::create('responses', function (Blueprint $table) {
        //     $table->increments("id");
        //     $table->string("status");
        //     $table->string("size");
        //     $table->longtext("headers");
        // });


        // Schema::create('requests', function (Blueprint $table) {
        //     $table->increments("id");
        //     $table->string("method");
        //     $table->string("uri");
        //     $table->string("url");
        //     $table->longtext("querystring");
        //     $table->string("size");
        //     $table->longtext("headers");
        // });

        Schema::create('logs', function (Blueprint $table) {
            $table->increments("id");
            $table->string("upstream_uri");
            $table->string("client_ip");
            $table->string("started_at");

            $table->string('consumer_id');

            // LATENCIES
            $table->integer("proxy");
            $table->integer("kong");
            $table->integer("request");

            // LATENCIES

            //    RESPONSE
            $table->string("response_status");
            $table->string("response_size");
            $table->longtext("response_headers");
            //    RESPONSE

            // REQUEST
            $table->string("request_method");
            $table->string("request_uri");
            $table->string("request_url");
            $table->longtext("request_querystring");
            $table->string("request_size");
            $table->longtext("request_headers");
            // REQUEST

            $table->string('route_id')->references('id')->on('routes');

            // $table->string('request_id')->references('id')->on('requests');

            // $table->string('response_id')->references('id')->on('responses');

            // $table->string('authenticated_entity_id')->references('id')->on('authenticated_entity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('latencies');
        Schema::dropIfExists('services');
        Schema::dropIfExists('consumers');
        Schema::dropIfExists('response_headers');
        Schema::dropIfExists('request_headers');
        Schema::dropIfExists('protocols');
        Schema::dropIfExists('methods');
        Schema::dropIfExists('paths');
        Schema::dropIfExists('routes');
        Schema::dropIfExists('route_protocol');
        Schema::dropIfExists('route_method');
        Schema::dropIfExists('route_path');
        Schema::dropIfExists('authenticated_entity');
        Schema::dropIfExists('responses');
        Schema::dropIfExists('requests');
        Schema::dropIfExists('requisicoes');
    }
}
