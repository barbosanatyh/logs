<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get("/logs", "App\Http\Controllers\LogsController@index");
Route::get("/show", "App\Http\Controllers\LogsController@show");

Route::get("/consumer", "App\Http\Controllers\LogsController@consumer");
Route::get("/service", "App\Http\Controllers\LogsController@service");
Route::get("/service_media", "App\Http\Controllers\LogsController@service_media");